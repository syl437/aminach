angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$rootScope,$ionicHistory,$ionicSideMenuDelegate,$state,$ionicLoading) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.checkUserLoggedIn = function()
  {
      if (window.localStorage.userid)
          $scope.userLoggedIn = true
      else
          $scope.userLoggedIn = false;
  }

  $scope.checkUserLoggedIn();

  $scope.checkAlreadyConnected = function()
  {
    $ionicHistory.nextViewOptions({
        disableAnimate: true,
        expire: 300,
        disableBack: true
    });

    if (window.localStorage.userid)
        $state.go('app.main');
  }


  $scope.CheckDisconnectUser = function()
  {
      if (!window.localStorage.userid)
        $state.go('app.login');
  }

  $scope.goBack = function()
  {
      $ionicHistory.goBack();
  }

  $scope.navigateHome = function()
  {
     $state.go('app.main');
  }


  $scope.$watch(function () {
    return $ionicSideMenuDelegate.getOpenRatio();
  },
    function (ratio) {
    if (ratio === -1){
      $rootScope.showMenuButton = false;
    } else{
        $rootScope.showMenuButton = true;
    }

  });

 $scope.toggleRightSideMenu = function()
 {
     $rootScope.showMenuButton = false;
    $ionicSideMenuDelegate.toggleRight();
 };

 $scope.navigatePage = function(page)
 {
     $state.go(page);
     //window.location ="#/app/"+page;
 }


  $scope.logOut = function()
  {
    window.localStorage.userid = '';
    $scope.userLoggedIn = false;

    $ionicHistory.nextViewOptions({
            disableAnimate: true,
            expire: 300,
            disableBack: true
        });

    $ionicSideMenuDelegate.toggleRight();
    $state.go('app.login');
  }




})

.controller('LoginCtrl', function($scope, $stateParams,$rootScope,$ionicPopup,$cordovaCamera,$timeout,$ionicHistory,$state,$ionicSideMenuDelegate,SendPostRequestServer) {

    $ionicSideMenuDelegate.canDragContent(false);

    $scope.checkAlreadyConnected();

    $scope.loginfields =
    {
        "username" : "",
        "password" : "" //"424242"
    }


    $scope.doLogin = function()
    {
        if ($scope.loginfields.username =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין שם משתמש',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else if ($scope.loginfields.password =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין סיסמה',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else
        {


            $scope.sendparams =
            {
                "u" : $scope.loginfields.username,
                "p" : $scope.loginfields.password
            }
            console.log($scope.sendparams)
            SendPostRequestServer.run($scope.sendparams,$rootScope.APIUrl+'/GetClientDetailsByUserPasswordNew').then(function(data)
            {

                  var x2js = new X2JS();
                  var data = x2js.xml_str2json(data);
                  var userid = data.DetailsBusiness.BusinessId;
                  var SystemRegion = data.DetailsBusiness.SystemRegion;
                  console.log ("login" , data)

                  if (userid == 0 || userid == undefined)
                  {
                        $ionicPopup.alert({
                         title: 'שם משתמש או סיסמה שגוים יש לנסות שוב',
                         template: '',
                        buttons: [{
                            text: 'אישור',
                            type: 'button-positive',
                          }]
                        });
                  }
                  else
                  {
                        window.localStorage.userid = userid;
                        $scope.checkUserLoggedIn();
                        
                        if (SystemRegion == "1")
                        {
                            window.localStorage.APIUrl =  "http://82.166.81.109:9897/Bookeep.asmx";
                            window.localStorage.uploadAPI = "http://82.166.81.109:8098/Service1.svc/";
                        }
                        else
                        {
                            window.localStorage.APIUrl =  "http://avitanz.com:8094/Bookeep.asmx/";
                            window.localStorage.uploadAPI = "http://avitanz.com:8095/Service1.svc/";                            
                        }

                        $ionicHistory.nextViewOptions({
                                disableAnimate: false,
                                expire: 300,
                                disableBack: true
                            });

                        $state.go('app.main');
                  }

            });
        }

    }


})


.controller('MainCtrl', function($scope, $stateParams,$rootScope,$ionicPopup,$cordovaCamera,$timeout,$ionicHistory,$state,$ionicSideMenuDelegate) {

    $scope.navTitle = "";//"<p>"+window.localStorage.fullname+"</p>";
    $scope.CheckDisconnectUser();
    //$ionicSideMenuDelegate.canDragContent(false);



})


.controller('ScanCtrl', function($scope, $stateParams,$rootScope,$ionicPopup,$cordovaCamera,$timeout,$ionicHistory,$state,SendPostRequestServer,SendPostRequestServer,FileService,$http,$ionicLoading,$ionicSideMenuDelegate) {

    $scope.navTitle = "";//"<p>"+window.localStorage.fullname+"</p>";
    $scope.CheckDisconnectUser();
    $scope.defaultProfileImage = 'img/in2.png';
    $scope.defaultExpenseImage = 'img/out2.png';
    $scope.showClientsSelect = false;
    //$ionicSideMenuDelegate.canDragContent(false);
    $scope.uploadedImages = [];
    $scope.uploadedImagesCount = 0;
    $scope.showSubmitButton = false;
    $scope.showSelectCompany = false;
    $scope.clientsArray = [];




    $scope.scanfileds =
    {
        "client_id" : "",
        "client_name" : "",
        "document_type" : ""
    }


    $scope.$watch('uploadedImages', function ()
    {
        if ($scope.uploadedImages.length > 0)
            $scope.showSubmitButton = true;
        else
            $scope.showSubmitButton = false;
    },true);


    $scope.setExpenseType = function(type)
    {
        if (type == 1)
        {
            $scope.defaultProfileImage = 'img/in1.png';
            $scope.defaultExpenseImage = 'img/out2.png';
        }
        else
        {
            $scope.defaultProfileImage = 'img/in2.png';
            $scope.defaultExpenseImage = 'img/out1.png';
        }

        $scope.scanfileds.document_type = type;
    }


    $scope.getClients = function()
    {
        
        $scope.sendparams =
        {
            "clinetID" : window.localStorage.userid,
        }

        SendPostRequestServer.run($scope.sendparams,window.localStorage.APIUrl+'/GetBsuinessListByClinetId').then(function(data)
        {
            $scope.clientsArray = [];
            var x2js = new X2JS();
            var data = x2js.xml_str2json(data);
            
            
            $scope.dataarray = data.ArrayOfEntityBusines.EntityBusines;
            
            if ($scope.dataarray.length == undefined)
            {
                $scope.clientsArray.push($scope.dataarray);
                $scope.selectClient(0);
            }
            else
            {
                $scope.showSelectCompany = true;
                $scope.clientsArray = $scope.dataarray;
                $scope.selectClient(0);
            }
                

            console.log("clients, ", $scope.clientsArray);
            //console.log("clients: ", data.ArrayOfEntityBusines.EntityBusines);

            //if ($scope.clientsArray.length == 1)
                //$scope.selectClient(0);

        });
    }


    $scope.getClients();

    $scope.selectClient = function(index)
    {
        
        if ($scope.dataarray.length == undefined)
        {
            $scope.scanfileds.client_id = $scope.clientsArray[0].BusinessId;
            $scope.scanfileds.client_name = $scope.clientsArray[0].BusinessName;    
        }
        else
        {
            $scope.scanfileds.client_id = $scope.clientsArray[index].BusinessId;
            $scope.scanfileds.client_name = $scope.clientsArray[index].BusinessName;            
        }
        
        


        /*
        for(var i = 0; i < $scope.clientsArray.length; i++)
        {
           $scope.clientsArray[i].isSelected = 0;
        }
        $scope.clientsArray[index].isSelected = 1;
        */

    }

    $scope.clearSelectedClients = function()
    {
        if ($scope.clientsArray.length > 0)
        {
            for(var i = 0; i < $scope.clientsArray.length; i++)
            {
               $scope.clientsArray[i].isSelected = 0;
            }
        }
    }


    $scope.sendDocument = function()
    {
        if ($scope.scanfileds.client_id =="")
        {
            $ionicPopup.alert({
             title: 'יש לבחור בית עסק',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else if ($scope.scanfileds.document_type =="")
        {
            $ionicPopup.alert({
             title: 'יש לבחור סוג מסמך',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else if ($scope.uploadedImages.length == 0)
        {

            $ionicPopup.alert({
             title: 'יש להעלאות לפחות מסמך אחד',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else
        {

            $ionicLoading.show({
              template: '<ion-spinner icon="lines" class="spinner-positive"></ion-spinner>',
              noBackdrop : false,
              duration : 10000
            });

            for(var i = 0; i < $scope.uploadedImages.length; i++)
            {
                //alert($scope.uploadedImages[i].ext)
                //alert($scope.uploadedImages[i].file)

                $scope.uploadedImagesCount++;


                var formData = new FormData();
                formData.append('file', $scope.uploadedImages[i].file);

                $http.post(window.localStorage.uploadAPI+'/UploadFile?BusinessId='+$scope.scanfileds.client_id+'&DocumentType='+$scope.scanfileds.document_type+'&ext='+$scope.uploadedImages[i].ext,formData,{headers: {"Content-Type": "multipart/form-data"}  })
                .then(function(data) {
                    $ionicLoading.hide();
                    //alert(JSON.stringify(data));


                    console.log("ok:", data);


                },function(error){
                    $ionicLoading.hide();
                    console.log("error:", error);
                })

            }

            if ($scope.uploadedImagesCount == $scope.uploadedImages.length)
            {
                $scope.uploadedImagesCount = 0;
                $scope.uploadedImages = [];

                $ionicPopup.alert({
                 title: 'תודה,מסמכים הועלו בהצלחה',
                 template: '',
                buttons: [{
                    text: 'אישור',
                    type: 'button-positive',
                  }]
                });
            }

        }

    }

    $scope.getFileExtension = function(filename)
    {
      var ext = /^.+\.([^.]+)$/.exec(filename);
      return ext == null ? "" : ext[1];
    }


    /* take picture */

    $scope.CameraOptions = function()
    {
        var myPopup = $ionicPopup.show({
        //template: '<input type="text" ng-model="data.myData">',
        //template: '<style>.popup { width:500px; }</style>',
        title: "בחירת אפשרות:",
        scope: $scope,
        cssClass: 'custom-popup',
        buttons: [


       {
        text: "מצלמה",
        type: 'button-positive',
        onTap: function(e) {
          $scope.takePicture(1);
        }
       },
       {
        text: "גלריית תמונות",
        type: 'button-calm',
        onTap: function(e) {
         $scope.takePicture(0);
        }
       },
              {
        text: "ביטול",
        type: 'button-assertive',
        onTap: function(e) {
          //alert (1)
        }
       },
       ]
      });
    }

    $scope.takePicture = function(index)
    {



         var options;

        if(index == 1 )
        {
            options = {
                quality : 60,
                destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL,
                sourceType : Camera.PictureSourceType.CAMERA,
                allowEdit : ionic.Platform.isIOS() ? true : true,
                encodingType: Camera.EncodingType.JPEG,
                //targetWidth: 600,
                //targetHeight: 600,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };
        }
        else
        {
            options = {
                quality : 60,
                destinationType : Camera.DestinationType.FILE_URI,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit : ionic.Platform.isIOS() ? true : true,
                encodingType: Camera.EncodingType.JPEG,
                //targetWidth: 600,
                //targetHeight: 600,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true
            };
        }

        $cordovaCamera.getPicture(options).then(function(imageData)
        {

            
            /*
            if(index == 1 )
            $scope.imgURI = "data:image/jpeg;base64," + imageData;
            else
            */
            $scope.imgURI = imageData
            var myImg = $scope.imgURI;

            
            
           $scope.sendImagesToArray = function(imagesource)
           {
                FileService.convertFileUriToFile(imagesource).then(function(newfile)
                {

                $scope.newfilename = $scope.imgURI.substr($scope.imgURI.lastIndexOf('/') + 1).split("?");
                $scope.fileext = $scope.getFileExtension($scope.newfilename[0]);

                $scope.uploadedImages.push({
                    "image" : imagesource,
                    "file" : newfile,
                    "ext" : $scope.fileext
                });

             });                   
           }

               
            
             /*Crop Image Plugin Code
            if (ionic.Platform.isIOS() == true)
            {


               plugins.crop(function success (data) {
                $scope.sendImagesToArray(data);
               }, 
               function fail () {
                   //$ionicLoading.hide();
                   $scope.sendImagesToArray(imageData);
               }, imageData, {quality:60}); 
            }
            else
            {
                $scope.sendImagesToArray(imageData);
            }
             */
             $scope.sendImagesToArray(imageData);
                                                

        });


    }

    /* take picture */

    $scope.deleteImage = function(index)
    {
        $scope.uploadedImages.splice(index, 1);
    }
    
    
    document.addEventListener("resume", $scope.getClients, false);

})


.controller('ReportCtrl', function($scope,$ionicModal, $stateParams,$rootScope,$ionicPopup,$cordovaCamera,$timeout,$ionicHistory,$state,SendPostRequestServer,SendGetRequestServer,$ionicScrollDelegate,$ionicLoading,$ionicSideMenuDelegate,dateFilter,$filter,$window) {

    //$ionicSideMenuDelegate.canDragContent(false);
    $scope.navTitle = "";//"<p>"+window.localStorage.fullname+"</p>";
    $scope.showSearchDiv = true;
    $scope.showResultsDiv = false;
    $scope.showSelectCompany = false;
    $scope.CheckDisconnectUser();
    $scope.reportArray = [];
    $scope.ActiveTab = 1;
    $scope.totalProfit = 0;
    $scope.totalExpense = 0;
    $scope.filteredArray = [];
    $scope.clientsArray = [];
    $scope.SortArray = [];
    $scope.SelectCategory = "";
    $scope.BarOpen = true;
    $scope.itemCounter = 0;
    $scope.totalVat = 0;
    $scope.calculateExpenses = 0;
    $scope.scrollheight = { height: ($window.innerHeight -300 ) + 'px' };

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10)
        dd='0'+dd

    if(mm<10)
        mm='0'+mm

    $scope.todaydate = dd+'/'+mm+'/'+yyyy;
    $scope.firstdaymonth = '01'+'/'+mm+'/'+yyyy;


    $scope.searchfields =
    {
        "businessId" : "",
        "dtaeFrom" : $scope.firstdaymonth,//"2016-01-01",
        "dateTo" :$scope.todaydate//"2017-07-19"
    }

    $scope.updateCounter = function(id1 , id2)
  {
    console.log("ID : " + id1 + " : " + id2)
    if(id1 == id2)
    {
      $scope.itemCounter++;
      console.log("CT : " + $scope.itemCounter)
      return $scope.itemCounter;
    }
    else
      return ''
  }


    $scope.showSearchFun = function()
    {
        $scope.showSearchDiv = true;
        $scope.showResultsDiv = false;
        $scope.totalProfit = 0;
        $scope.totalExpense = 0;
        $scope.itemCounter = 0;
        $scope.totalVat = 0;
        $scope.calculateExpenses = 0;    
    }

    $scope.setActiveTab = function(tab)
    {
        $scope.ActiveTab = tab;
        $ionicScrollDelegate.scrollTop();
    }

    $scope.getClients = function()
    {
        $scope.sendparams =
        {
            "clinetID" : window.localStorage.userid,
        }

        SendPostRequestServer.run($scope.sendparams,window.localStorage.APIUrl+'/GetBsuinessListByClinetId').then(function(data)
        {

            $scope.clientsArray = [];
            var x2js = new X2JS();
            var data = x2js.xml_str2json(data);
            
            
            $scope.dataarray = data.ArrayOfEntityBusines.EntityBusines;
            
            if ($scope.dataarray.length == undefined)
            {
                $scope.clientsArray.push($scope.dataarray);
                $scope.selectClient(0);
            }
            else
            {
                $scope.showSelectCompany = true;
                $scope.clientsArray = $scope.dataarray;
                $scope.selectClient(0);
            }
                

            console.log("clients, ", $scope.clientsArray);
            //console.log("clients: ", data.ArrayOfEntityBusines.EntityBusines);

            //if ($scope.clientsArray.length == 1)
                //$scope.selectClient(0);


        });
    }


    $scope.getClients();

    $scope.selectClient = function(index)
    {
        
        if ($scope.dataarray.length == undefined)
        {
            $scope.searchfields.businessId =  $scope.dataarray.BusinessId;
        }
        else
        {
            $scope.dataarray[index].isSelected = 1;
            $scope.searchfields.businessId = $scope.dataarray[index].BusinessId;            
        }
        //alert ($scope.searchfields.businessId)
        //$scope.clientSelected = true;
    }

  $scope.changeCategory = function(Cat)
  {
    $scope.itemCounter = 0;
    if($scope.SelectCategory == Cat)
    {
      console.log("1")
      if($scope.BarOpen == false)
        $scope.BarOpen = true;
      else
        $scope.BarOpen = false;
    }
    else
    {
      console.log("2 : " + $scope.BarOpen)
      $scope.SelectCategory = Cat;
      $scope.BarOpen = true;
    }
  }
  
  
  $scope.getTotal = function (ID) {

      var Total = 0;
      var TotalAmount = 0;

    for (var i = 0; i < $scope.reportArray.length; i++) {
      if ($scope.reportArray[i].ExpenseId == ID && $scope.reportArray[i].SugPkuda == $scope.ActiveTab)
      {
        TotalAmount+= Number($scope.reportArray[i].TotalPrice);
        //console.log("GetTotal : " + $scope.reportArray[i].TotalPrice + " : " +  $scope.reportArray[i].ExpenseName + " : "  +$scope.reportArray[i].SugPkuda)
      }
    }

      //console.log("GetTotalEnd : " + TotalAmount);
    /*
    TotalAmount = Number(TotalAmount)
    TotalAmount = ("" + parseFloat(TotalAmount)).substring(0, 5);
    TotalAmount.toLocaleString();
    TotalAmount.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    */
    return TotalAmount.toString();
    //return Total.toString()+'/'+TotalAmount.toString();
  }

  $scope.getCounter = function (i) {
    $scope.itemCounter++
    console.log("I :: " , i)
    return $scope.itemCounter;
  };


  $scope.searchDate = function()
    {
        if ($scope.searchfields.businessId =="")
        {
            $ionicPopup.alert({
             title: 'יש תחילה לבחור עסק',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }

        else if ($scope.searchfields.dtaeFrom =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין תאריך התחלה',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else if ($scope.searchfields.dateTo =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין תאריך סיום',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });
        }
        else
        {
            $scope.profitArray = [];
            /* $scope.NewDateFrom = $scope.searchfields.dtaeFrom.split('/');
            $scope.NewDateFrom = $scope.NewDateFrom[2]+"-"+$scope.NewDateFrom[1]+"-"+$scope.NewDateFrom[0];
            $scope.NewDateTo = $scope.searchfields.dateTo.split('/');
            $scope.NewDateTo = $scope.NewDateTo[2]+"-"+$scope.NewDateTo[1]+"-"+$scope.NewDateTo[0];*/

            $scope.newstartdate = $scope.searchfields.dtaeFrom.split('/').reverse().join('-');
            $scope.newenddate = $scope.searchfields.dateTo.split('/').reverse().join('-');


            //$scope.newstartdate = dateFilter($newstartdate, 'yyyy-MM-dd');
            //$scope.newenddate = dateFilter($scope.newenddate, 'yyyy-MM-dd');


            $scope.sendfields =
            {
                "businessId" : $scope.searchfields.businessId,
                "dtaeFrom" : $scope.newstartdate, //'2016-08-10'
                "dateTo" : $scope.newenddate //'2017-08-10'
            }


            console.log("sendfields: " , $scope.sendfields);


            //GetListRequestRoomAvailable
            SendPostRequestServer.run($scope.sendfields,window.localStorage.APIUrl+'/GetJournalEntriesByDateAndBusinessIDNew').then(function(data)
            {
                var x2js = new X2JS();
                var data = x2js.xml_str2json(data);
                console.log ("resultsNNNN",data);

                //$scope.searchresults = [];
                $scope.reportArray = data.JournalEntriesAndSamVat.lstJournalEntries.JournalEntries;
                
                
                //$scope.searchresults = data.JournalEntriesAndSamVat.lstJournalEntries;
                //alert ($scope.searchresults.length)
                //alert ($scope.searchresults);
                
                //console.log ("$scope.reportArray",$scope.reportArray);
                if ($scope.reportArray == undefined || $scope.reportArray.length == undefined)
                {
                    $ionicPopup.alert({
                     title: 'לא נמצאו תוצאות יש לנסות תאריך אחר',
                     template: '',
                    buttons: [{
                        text: 'אישור',
                        type: 'button-positive',
                      }]
                    });
                }
                else
                {
                  for (var i = 0; i < $scope.reportArray.length; i++)
                  {
                        $scope.isAvaileble = 0;
                        for (var j = 0; j < $scope.SortArray.length; j++) {
                            
                            if($scope.SortArray[j].id == $scope.reportArray[i].ExpenseId && $scope.SortArray[j].SugPkuda == $scope.reportArray[i].SugPkuda) 
                            {
                              $scope.isAvaileble = 1;
                            }
                    }

                    if($scope.isAvaileble == 0)
                    {
                        var Obj = new Object;
                        Obj.id = $scope.reportArray[i].ExpenseId;
                        Obj.name = $scope.reportArray[i].ExpenseName;
                        Obj.SugPkuda = $scope.reportArray[i].SugPkuda;

                       $scope.SortArray.push(Obj)
                       //$scope.SortArray=$filter('unique')($scope.SortArray,"id");
                    }

                    $scope.isAvaileble = 0;

                    if ($scope.reportArray[i].SugPkuda == 1)
                        $scope.profitArray.push($scope.reportArray[i]);


                    //if ($scope.reportArray[i].SugPkuda == 1)
                      //$scope.totalProfit = $scope.totalProfit + Number($scope.reportArray[i].TotalPrice);

                    //if ($scope.reportArray[i].SugPkuda == -1)
                      //$scope.totalExpense = $scope.totalExpense + Number($scope.reportArray[i].TotalPrice);
                  
                    //$scope.totalVat = $scope.totalVat+Number($scope.reportArray[i].Vat);

                    //$scope.clientsArray[i].isSelected = 0;

                  }


                      $scope.totalProfit = Number(data.JournalEntriesAndSamVat.SumIncome);
                      $scope.totalExpense = Number(data.JournalEntriesAndSamVat.SumExpense);
                      $scope.totalVat = Number(data.JournalEntriesAndSamVat.SumVat);
                      $scope.calculateExpenses = Number($scope.totalProfit)-Number($scope.totalExpense);
                  
                     //console.log("$scope.calculateExpenses",$scope.calculateExpenses);
                    //console.log("profitArray : ", $scope.profitArray)
                    //console.log("reportArray : ", $scope.reportArray)
                    //console.log("SortArray : ", $scope.SortArray)
                    //console.log("$scope.filteredArray", $scope.filteredArray)

                    //console.log("totalProfit", $scope.totalProfit);
                    //console.log("totalExpense", $scope.totalExpense);

                    $scope.showSearchDiv = false;
                    $scope.showResultsDiv = true;
          }


                //console.log("search results: ", data);

            });
        }

    }
    
    $scope.currentItem = '';
    document.addEventListener("resume", $scope.getClients, false);

    $scope.openImage = function(item)
    {
        $scope.currentItem = item;

        console.log("item1 : " , $scope.currentItem )

        if($scope.currentItem.DocPath != '')
        {
            $ionicModal.fromTemplateUrl('templates/modal.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
                $scope.modal.show();
            });
        }
        else
        {
            alert("לא קיים מסמך לפריט זה")
        }

        //alert(ImagPath)
    }



})

.controller('UpdateInfoCtrl', function($scope, $stateParams,$rootScope,$ionicPopup,$cordovaCamera,$timeout,$ionicHistory,$state,SendPostRequestServer,SendGetRequestServer,$ionicScrollDelegate,$ionicLoading,$ionicSideMenuDelegate,dateFilter,$filter,$window) {
    
    $scope.passwordfields = 
    {
        "oldpassword" : "",
        "newpassword" : ""

        
    }

    $scope.changePassword = function()
    {
        
        if ($scope.passwordfields.oldpassword =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין סיסמה ישנה',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });            
        }
        else if ($scope.passwordfields.newpassword =="")
        {
            $ionicPopup.alert({
             title: 'יש להזין סיסמה חדשה',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });            
        }
        else if ($scope.passwordfields.newpassword.search(/.*[a-z]{2}.*/i) === -1) 
        {
            
            $ionicPopup.alert({
             title: 'סיסמה חדשה חייבת להכין לפחות 2 תווים באנגלית',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });    
            $scope.passwordfields.newpassword = '';
        }        

        
        else if ($scope.passwordfields.newpassword.length > 8)
        {
            $ionicPopup.alert({
             title: 'סיסמה חדשה לא יכולה להיות יותר מ 8 תווים',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });    
            $scope.passwordfields.newpassword = '';
        }
        else if ($scope.passwordfields.newpassword.length < 6)
        {
            $ionicPopup.alert({
             title:'סיסמה חדשה לא יכולה להכיל פחות מ 6 תווים',
             template: '',
            buttons: [{
                text: 'אישור',
                type: 'button-positive',
              }]
            });    
            $scope.passwordfields.newpassword = '';
        }            
        else
        {
            
            $scope.sendfields =
            {
                "clinetID" : window.localStorage.userid,
                "passwordOld" :  $scope.passwordfields.oldpassword,
                "passwordNew" :  $scope.passwordfields.newpassword,
            }

            SendPostRequestServer.run($scope.sendfields,window.localStorage.APIUrl+'/UpdatePasswordByClientIdAndPasswordOld').then(function(data)
            {

            

                    var x2js = new X2JS();
                    var data = x2js.xml_str2json(data);
                    var response = data.boolean;
                     
                    
                    if (response =="true")
                    {
                        $ionicPopup.alert({
                         title: 'סיסמה שונתה בהצלחה',
                         template: '',
                        buttons: [{
                            text: 'אישור',
                            type: 'button-positive',
                          }]
                        });    
                        
                        $scope.passwordfields.oldpassword = '';
                        $scope.passwordfields.newpassword = '';
                        $state.go('app.main');
                    }
                    else
                    {
                        $ionicPopup.alert({
                         title: 'סיסמה ישנה שגויה יש לנסות שוב',
                         template: '',
                        buttons: [{
                            text: 'אישור',
                            type: 'button-positive',
                          }]
                        });                        
                        $scope.passwordfields.oldpassword = '';
                    }                    
                    
                

            


                console.log("forot: ", data);

            });


            

        }
    }

})



.filter('unique', function() {

  return function (arr, field) {
    var o = {}, i, l = arr.length, r = [];
    for(i=0; i<l;i+=1) {
      o[arr[i][field]] = arr[i];
    }
    for(i in o) {
      r.push(o[i]);
    }
    return r;
  };
})